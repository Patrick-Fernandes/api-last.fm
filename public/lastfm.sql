-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 18-Dez-2018 às 17:16
-- Versão do servidor: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lastfm`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `musica`
--

CREATE TABLE `musica` (
  `Id` int(11) NOT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Artist` varchar(300) DEFAULT NULL,
  `Url` varchar(300) DEFAULT NULL,
  `Image` varchar(300) DEFAULT NULL,
  `NameMusic` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `musica`
--

INSERT INTO `musica` (`Id`, `Name`, `Artist`, `Url`, `Image`, `NameMusic`) VALUES
(19, 'BromÃ©lias', 'BidÃª ou Balde', 'https://www.last.fm/music/Bid%C3%AA+ou+Balde/_/Brom%C3%A9lias', 'https://lastfm-img2.akamaized.net/i/u/300x300/edb52ed9a500411e955c5427905ef582.png', 'BidÃª ou Balde - BromÃ©lias.mp3'),
(20, 'Abre-Te SÃ©samo', 'Raul Seixas', 'https://www.last.fm/music/Raul+Seixas/_/01(Raul+Seixas)+-+Abre-Te+S%C3%A9samo', 'https://lastfm-img2.akamaized.net/i/u/300x300/9355e2481a8be71378351e0a1557ced5.png', '01 Abre-Te SÃ©samo.mp3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `musica`
--
ALTER TABLE `musica`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `musica`
--
ALTER TABLE `musica`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
