<?php

namespace App\Classe\DAL;

use App\Classe\DB as DB;

class MusicaDAL {

    public function __construct()
    {
        
    }

    /**
     * Está função serve para salvar os dados da música no banco de dados
     * @param objeto $obj Esta variavel recebe um objeto com os dados para serem salvos no banco.
     * @return boolean dizendo se foi salvo ou não foi possivel salvar

     */
    public function salvarMusica($obj)
    {
        //criar um objeto DB para poder fazer a conexão como banco de dados
        $objdb = new DB;
        $str = "
                INSERT INTO `musica`
                (`Name`, `Artist`, 
                `Url`, `Image`, `NameMusic`)
                VALUES (
                '" . $obj->getName() . "',
                '" . $obj->getArtist() . "',
                '" . $obj->getUrl() . "',
                '" . $obj->getImage() . "',
                '" . $obj->getNameMusic() . "')";
        $query = mysqli_query($objdb->LigarConexao(), $str);
        if ($query)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Função para deletar uma música do banco de dados.
     * @param int $id Id para especificar qual música será deletada.
     * @return boolean dizendo se foi deletado ou se não foi possivel deletar.
     */
    public function deletarMusica($id)
    {
        $objdb = new DB;

        $str = "DELETE FROM musica WHERE ID = " . $id;

        $query = mysqli_query($objdb->LigarConexao(), $str);

        if ($query)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Função para trazer todos os dados das musicas
     * @return $quary caso ache algum dado no banco de dados ele retorna uma Array com todos os daos encontrados.
     */
    public function buscarMusica()
    {
        $objdb = new DB;

        $str = "SELECT * FROM musica";

        $query = mysqli_query($objdb->LigarConexao(), $str);

        if ($query)
        {
            return $query;
        }
        else
        {
            return false;
        }
    }

}
