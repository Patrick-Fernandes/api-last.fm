<?php

namespace App\Classe\TO;

/**
 * 
 */
class MusicaTO {

    private $Id;
    private $Name;
    private $Artist;
    private $Url;
    private $Image;
    private $NameMusic;

    /** Função contrutor da MusicaTO
     * 
     * @param int $Id id da msuica. 
     * @param String $Name nome da musica. 
     * @param String $Artist nome do artista.
     * @param String $Url link da música na Last.FM.
     * @param String $Image link da imagem de capa
     * @param String $NameMusic nome do arquivo de música.
     */
    public function __construct($Id = "", $Name = "", $Artist = "", $Url = "", $Image = "", $NameMusic = "")
    {
        $this->Id = $Id;
        $this->Name = $Name;
        $this->Artist = $Artist;
        $this->Url = $Url;
        $this->Image = $Image;
        $this->NameMusic = $NameMusic;
    }

    public function getId()
    {
        return $this->Id;
    }

    public function getName()
    {
        return $this->Name;
    }

    public function getArtist()
    {
        return $this->Artist;
    }

    public function getUrl()
    {
        return $this->Url;
    }

    public function getImage()
    {
        return $this->Image;
    }

    public function getNameMusic()
    {
        return $this->NameMusic;
    }

    public function setId($Id)
    {
        $this->Id = $Id;
    }

    public function setName($Name)
    {
        $this->Name = $Name;
    }

    public function setArtist($Artist)
    {
        $this->Artist = $Artist;
    }

    public function setUrl($Url)
    {
        $this->Url = $Url;
    }

    public function setImage($Image)
    {
        $this->Image = $Image;
    }

    public function setNameMusic($NameMusic)
    {
        $this->NameMusic = $NameMusic;
    }

}
