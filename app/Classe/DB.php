<?php

namespace App\Classe;

class DB {

    private $server;
    private $usuario;
    private $senha;
    private $banco;
    private $conn;
    private $msgErroQuery;
    private $query;

    /**
     * Método contrtutor do banco de dados
     */
    public function __construct()
    {
        self::escreveDados($this->server, "localhost");
        self::escreveDados($this->usuario, "root");
        self::escreveDados($this->senha, "");
        self::escreveDados($this->banco, "LastFM");
        self::conexao();
    }

    /**
     * Função que faz a ligação com o banco de dados.
     * 
     */
    private function conexao()
    {
        $conect = mysqli_connect($this->server, $this->usuario, $this->senha) or
                die("Não foi possivel conectar ao servidor mysql.<br>" . mysqli_error($this->conn));
        $this->conn = $conect;
        self::selecionaDB();
    }

    /**
     * Função para ser usada em classes mysqli. Que nescessitem o envio da conexão do banco
     * @return Conexão do banco de dados
     */
    public function LigarConexao()
    {
        return $this->conn;
    }

    /**
     * Função para fechar as coneções do banco de dados.
     * 
     * @return Fechamento da conexão
     */
    public function exitConexao()
    {
        return mysqli_close($this->conn);
    }

    /**
     * Funão que faz a conexão de selecionar o banco de dados.
     */
    private function selecionaDB()
    {
        mysqli_select_db($this->conn, $this->banco) or
                die("Não foi possivel selecionar a base de dados.<br>" . mysqli_error($this->conn));
    }

    /**
     *Função que escreve os dados para as variaveis
     *  
     * @param type $var
     * @param type $param
     * @return os parametros
     */
    private function escreveDados(&$var, $param)
    {
        return $var = $param;
    }

}
