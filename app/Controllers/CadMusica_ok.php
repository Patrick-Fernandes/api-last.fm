<?php
/**
 * Essa página serva para fazer o evnio finnal dos dados para o banco. 
 */
namespace App\Controllers;

require_once '../../vendor/autoload.php';

use App\Classe\DAL\MusicaDAL as MusicaDAL;
use App\Classe\TO\MusicaTO as MusicaTO;

if (empty($_POST['NameMusic']))
{
    echo '<script>'
    . '  alert("Por Favor, preencha todos os campos.");'
    . '  history.go(-1);'
    . '</script>';
}
else
{
    $nome = $_POST['Nome'];
    $nomeMusica = $_POST['NameMusic'];
    $artista = $_POST['Artista'];
    $image = $_POST['Image'];
    $Url = $_POST['Url'];

    $objTO = new MusicaTO;

    $objTO->setName($nome);
    $objTO->setNameMusic($nomeMusica);
    $objTO->setArtist($artista);
    $objTO->setUrl($Url);
    $objTO->setImage($image);
    $objDAL = new MusicaDAL;

    if ($objDAL->salvarMusica($objTO))
    {

        echo "<script>"
        . "  alert('Cadastro realizado com sucesso.');"
        . "  location.href = 'Home';"
        . "</script>";
    }
    else
    {

        echo "<script>"
        . "  alert('Erro ao salvar. Contate o administrador do sistema.');"
        . "  history.go(-1);"
        . "</script>";
    }
}
?>
