<?php

namespace App\Controllers;

require_once '../../vendor/autoload.php';

/**
 * Classe que faz o envio do arquivo .mp3 para a aplixação
 */
class UploadMusica {

    /**
     * Função que faz envio do arquivo .mp3 para a aplicação e traz um retorno de 10 músicas achadas na API
     * da Last.FM
     * @param String $tempName Local temporário do arquivo
     * @param String $nome nome original do arquivo
     * @param String $tipo tipo de arquivo
     * @param int $tamanho tamanho em bits do arquivo
     * @param boolean $erro erro de arquivo
     * @return string retorno de enlace de dados para serem selecionados qual música será cadastrada
     */
    public function UploadMusicaVarios($tempName, $nome, $tipo, $tamanho, $erro)
    {
        if ($erro == 0)
        {
            $target_dir = "../../public/Audio/";
            $fileName = $nome;
            $target_file = $target_dir . basename($fileName);
            $fileTemp = $tempName;
            $uploadOk = 1;
            $musicFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// checa se o arquivo já existe
            if (file_exists($target_file))
            {
                return "<p>Desculpa mas já existe um arquivo com este nome</p>"
                        . "<br><a class='btn btn-danger' href='app/Controllers/DeletaArquivo.php?&Link=" . base64_encode($fileName) . "'>Deseja Apagar a Musica do Sistema?</a></p>";
                ;
                $uploadOk = 0;
            }
// checa tamanho do arquivo
            if ($tamanho > 50000000)
            {
                return "<p>Arquivo grande de mais para enviar.</p>";
                $uploadOk = 0;
            }
// checa o formato do arquivo
            if ($musicFileType != "wma" && $musicFileType != "mp3")
            {
                return "<p>Formato incorreto de arquivo, somente arquivos .wma ou .mp3.</p>";
                $uploadOk = 0;
            }
// checa se existe algum problema de envio
            if ($uploadOk == 0)
            {
                return "<p>Desculpe, mas não foi possivel enviar o arquivo.</p>";
            }
            else
            {
                //move para a pasta determinada
                if (move_uploaded_file($fileTemp, $target_file))
                {

                    $Last = new LastFM;
                    $pieces = explode(".", $fileName);
                    return $Last->busacaMusica($pieces[0], $fileName);
                }
                else
                {
                    return "<p><B>Desculpa mas este arquivo já está no sistema.</b>"
                            . "<br><a class='btn btn-danger' href='app/Controllers/DeletaArquivo.php?&Link=" . base64_encode($fileName) . "'>Deseja Apagar a Musica do Sistema?</a></p>";
                }
            }
        }
    }

    /**
     * Função que faz envio do arquivo .mp3 para a aplicação e traz um retorno da primeira música encontrada na API
     * da Last.FM
     * @param String $tempName Local temporário do arquivo
     * @param String $nome nome original do arquivo
     * @param String $tipo tipo de arquivo
     * @param int $tamanho tamanho em bits do arquivo
     * @param boolean $erro erro de arquivo
     * @return string retorna um formulário com os dados da música achada
     */
    public function UploadMusica($tempName, $nome, $tipo, $tamanho, $erro)
    {
        if ($erro == 0)
        {
            $target_dir = "../../public/Audio/";
            $fileName = $nome;
            $target_file = $target_dir . basename($fileName);
            $fileTemp = $tempName;
            $uploadOk = 1;
            $musicFileType = strtolower(pathinfo($target_file, PATHINFO_EXTENSION));

// checa se o arquivo já existe
            if (file_exists($target_file))
            {
                return "<p>Desculpa mas já existe um arquivo com este nome</p>"
                        . "<br><a class='btn btn-danger' href='app/Controllers/DeletaArquivo.php?&Link=" . base64_encode($fileName) . "'>Deseja Apagar a Musica do Sistema?</a></p>";
                ;
                $uploadOk = 0;
            }
// checa tamanho do arquivo
            if ($tamanho > 50000000)
            {
                return "<p>Arquivo grande de mais para enviar.</p>";
                $uploadOk = 0;
            }
// checa o formato do arquivo
            if ($musicFileType != "wma" && $musicFileType != "mp3")
            {
                return "<p>Formato incorreto de arquivo, somente arquivos .wma ou .mp3.</p>";
                $uploadOk = 0;
            }
// checa se existe algum problema de envio
            if ($uploadOk == 0)
            {
                return "<p>Desculpe, mas não foi possivel enviar o arquivo.</p>";
            }
            else
            {
                //move para a pasta determinada
                if (move_uploaded_file($fileTemp, $target_file))
                {

                    $Last = new LastFM;
                    $pieces = explode(".", $fileName);
                    return $Last->busacaMusicaUnica($pieces[0], $fileName);
                }
                else
                {
                    return "<p><B>Desculpa mas este arquivo já está no sistema.</b>"
                            . "<br><a class='btn btn-danger' href='app/Controllers/DeletaArquivo.php?&Link=" . base64_encode($fileName) . "'>Deseja Apagar a Musica do Sistema?</a></p>";
                }
            }
        }
    }

}
