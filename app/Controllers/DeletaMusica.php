<?php

require_once '../../vendor/autoload.php';

use App\Classe\DAL\MusicaDAL as MusicaDAL;

//verifica se o id enviado é mesmo numerico e se existe
if (!is_numeric(base64_decode($_GET["id"])))
{
    ?>
    <script language="javascript">
        history.back(-1);
    </script>
    <?php

}
$id = base64_decode($_GET["id"]);

$Link = base64_decode($_GET["Link"]);

$musicaDAL = new MusicaDAL;
$retorno = $musicaDAL->buscarMusica("where id=$id");
if ($retorno)
{
    $retorno = $musicaDAL->deletarMusica($id);
    $cab = "../../public/Audio/" . $Link;
//verifica se caminho existe
    if (is_file($cab))
    {
        unlink($cab);
    }
    if ($retorno)
    {
        $msg = "<script>
alert('Dado Deletado');    
window.location.href = '../../Musica';
</script>";

        echo $msg;
    }
    else
    {
        $msg = "<script>
            alert('Não foi possível excluir. Verifique as dependências');
            history.back(-1);
            </script>";

        echo $msg;
    }
}
Musica
?>
