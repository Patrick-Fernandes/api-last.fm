<?php

namespace App\Controllers;

/**
 * Classe que faz a busca na API da Last.FM
 */
class LastFM {

    /**
     * Função que faz a busca na API e traz 10 resultados
     * @param String $search  nome original da música sem extensão
     * @param String $nomeMusica nome original da música com extensão
     * @return string retorna 10 formularios, com as 10 primeiras músicas para o usário escolher qual a melhor opção para cadastrar.
     */
    public function busacaMusica($search, $nomeMusica)
    {
        $extra = "limit=10";
        $link = "http://ws.audioscrobbler.com/2.0/?method=track.search&track=$search&api_key=aefa54108190e9c1d8dd093e5f628a21&$extra&format=json";
        $json_file = file_get_contents($link);
        $json_str = json_decode($json_file, true);
        $itens = $json_str['results']['trackmatches']['track'];
        $count = count($itens);
        $return = " ";
        $i = 0;
        if ($count > 0)
        {
            while ($count != $i)
            {
                $nome = $itens["$i"]['name'];
                $artista = $itens["$i"]['artist'];
                $url = $itens["$i"]['url'];
                $listeners = $itens["$i"]['listeners'];
                $image = $itens["$i"]['image'][3]["#text"];
                $nomeMusica = $nomeMusica;
                $return .= "               
<form action='EnviarMusicaBanco' method='post' enctype='multipart/form-data'>
<div class='form-group'><p><b>Nome:</b> 
    <input type='text' name='Nome' value='$nome'>
    <BR></p> </div><div class='form-group'>    <p><b>Artista:</b>
    <input type='text' name='Artista' value='$artista'>
    <BR></p> </div><div class='form-group'>    <p><b>Url:</b>
    <input type='text' name='Url' value='$url'>
    <BR></p></div><div class='form-group'>    <p><b>Vezes Ouvido</b>:   $listeners
    <BR></p> </div><div class='form-group'>    <p><b>Capa</b>:
    <img src='$image' size='small' width='150' height='150'>
    </p> </div><div class='form-group'><input type='hidden' name='Image' value='$image'>
    <input type='hidden' name='NameMusic' value='$nomeMusica'>
  <input type='submit' class='btn btn-sucess' value='Submit'></div></form>";
                $i++;
            }
            return $return . "<br><Br><br> <p><a class='btn btn-danger' href='app/Controllers/DeletaArquivo.php?&Link=" . base64_encode($nomeMusica) . "'>Deseja Apagar a Musica do Sistema?</a></p>";
        }
        else
        {
            return "<p><B>Não foram encontradas nenhuma musica na Last.Fm com este nome.</b><br><a class='btn btn-danger' href='app/Controllers/DeletaArquivo.php?&Link=" . base64_encode($nomeMusica) . "'>Deseja Apagar a Musica do Sistema?</a></p>";
        }
    }

    /**
     * Função que faz a busca na API e traz 1 resultado
     * @param String $search  nome original da música sem extensão
     * @param String $nomeMusica nome original da música com extensão
     * @return string retorna 1 formulario, com a primeira música encontrada  para cadastrar.
     */
    public function busacaMusicaUnica($search, $nomeMusica)
    {
        $extra = "limit=1";
        $link = "http://ws.audioscrobbler.com/2.0/?method=track.search&track=$search&api_key=aefa54108190e9c1d8dd093e5f628a21&$extra&format=json";
        $json_file = file_get_contents($link);
        $json_str = json_decode($json_file, true);
        $itens = $json_str['results']['trackmatches']['track'];
        $count = count($itens);
        $return = " ";
        $i = 0;
        if ($count > 0)
        {
            $nome = $itens["$i"]['name'];
            $artista = $itens["$i"]['artist'];
            $url = $itens["$i"]['url'];
            $listeners = $itens["$i"]['listeners'];
            $image = $itens["$i"]['image'][3]["#text"];
            $nomeMusica = $nomeMusica;
            $return = "               
<form action='EnviarMusicaBanco' method='post' enctype='multipart/form-data'>
<div class='form-group'><p><b>Nome:</b> 
    <input type='text' name='Nome' value='$nome'>
    <BR></p> </div><div class='form-group'>    <p><b>Artista:</b>
    <input type='text' name='Artista' value='$artista'>
    <BR></p> </div><div class='form-group'>    <p><b>Url:</b>
    <input type='text' name='Url' value='$url'>
    <BR></p></div><div class='form-group'>    <p><b>Vezes Ouvido</b>:   $listeners
    <BR></p> </div><div class='form-group'>    <p><b>Capa</b>:
    <img src='$image' size='small' width='150' height='150'>
    </p> </div><div class='form-group'><input type='hidden' name='Image' value='$image'>
    <input type='hidden' name='NameMusic' value='$nomeMusica'>
  <input type='submit' class='btn btn-sucess' value='Submit'></div></form>";
            $i++;
            return $return . "<br><Br><br> <p><a class='btn btn-danger' href='app/Controllers/DeletaArquivo.php?&Link=" . base64_encode($nomeMusica) . "'>Deseja Apagar a Musica do Sistema?</a></p>";
        }
        else
        {
            return "<p><B>Não foram encontradas nenhuma musica na Last.Fm com este nome.</b><br><a class='btn btn-danger' href='app/Controllers/DeletaArquivo.php?&Link=" . base64_encode($nomeMusica) . "'>Deseja Apagar a Musica do Sistema?</a></p>";
        }
    }

}
