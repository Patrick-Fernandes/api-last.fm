<?php

namespace App\Controllers;
use App\Classe\DAL\MusicaDAL as MusicaDAL;
/**
 * Classe que faz chama as buscas do banco de dados, na tabela Música.
 */
class BuscaMusica {

    /**
     * Função que chama a busca das músicas do banco de dados 
     * @return $objeto Retorna todos os registros achados no banco de dados.
     */
    function buscarMusicas()
    {
        $objDAL = new MusicaDAL;
        $sql = $objDAL->buscarMusica();

        return $sql;
    }

  

}
