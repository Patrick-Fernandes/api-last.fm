
<?php 
require_once '../../vendor/autoload.php'; ?>
<!DOCTYPE html>
<html>
    <head>
        <title>API Last.FM</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
  
        <link rel="stylesheet" href="public/Style/estilo.css" type="text/css" />
        <script type="text/javascript" src="public/Style/funcao.js"></script>
    </head>
    <body>
        <div class="armacao">

            <div class="cabecalho">
                <div class="info">
                    <h1>
                        Last.FM Api de Busca
                    </h1>
                </div>
            </div>
            <div class="corpo">
                <div class="menuContainer">
                    <ul class="menu">
                        <li>
                            <a href="Home">Home</a>
                        </li>
                        <li>
                            <a href="#">Buscas</a>
                            <ul>
                                <li><a href="UploadMusica">Busca uma música</a></li>
                                <li><a href="UploadMusicaVarias">Busca 10 músicas</a></li>
                            </ul>
                        </li>
                        <li>
                            <a href="Musica">Lista de Musicas</a>
                        </li>
                    </ul>
                </div>
                <div class="conteudo">

