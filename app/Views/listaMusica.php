<?php include 'incCabecalho.php'; ?>
<div class="itens">
    <div id="page-wrapper">
        <div class="container-fluid">
            <h3>Lista de Musicas</h3>
            <p>
                Lista de todas as músicas cadastradas no banco de dados
            </p>
            <div class="row">
                <div class="col-lg-12">
                    <table class="listas">
                        <thead>
                            <tr>
                                <td>Código</td>
                                <td>Nome</td>
                                <td>Artista</td>
                                <td>Url</td>
                                <td>Imagem</td>                               
                                <td>Música</td>
                                <td>Ações</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            use App\Controllers\BuscaMusica as BMusica;
                            $BMusica = new BMusica;
                            $sql = $BMusica->buscarMusicas();
                            while ($linha = mysqli_fetch_array($sql))
                            {
                                ?>
                                <tr>
                                    <td class="codigo">
                                        <?= $linha['Id'] ?>
                                    </td>
                                    <td>
                                        <?= $linha['Name'] ?>
                                    </td>
                                    <td>
    <?= $linha['Artist'] ?>
                                    </td>  
                                    <td>
                                        <a href="<?= $linha['Url'] ?>"> Link da Last.FM</a>
                                    </td>
                                    <td>
                                        <img src="  <?= $linha['Image'] ?>"
                                             size='small' width='100' height='100'>
                                    </td>
                                    <td>
                                        <audio controls>
                                            <source 
                                                src="public/Audio/<?= $linha['NameMusic'] ?>"
                                                type="audio/mpeg">
                                        </audio>                              
                                    </td>
                                    <td>
                                        <a class="btn btn-danger" href="app/Controllers/DeletaMusica.php?id=<?= base64_encode($linha['Id'])."&Link=". base64_encode($linha['NameMusic']) ?>">Excluir</a>
                                    </td>
                                </tr>
                              <?php
                            }
                            ?>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</div>
<?php
include 'incRodape.php';
