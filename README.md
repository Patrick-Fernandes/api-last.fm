# API Last.FM

Aplicativo de cadastro de músicas com API  da [Last.FM](https://www.last.fm/api/intro) 


## Instalação

Faça um banco de dados com o nome lastfm

Depois suba o arquivo .sql que se encontra na pasta public/

```bash
public/lastfm.sql
```


## Modo de uso do sistema

Na página inicial tem um menu onde haverá 3 links.


```bash

1 - Home
2-  Buscas
    2.1 - Busca uma música
    2.2 - Busca 10 músicas
3 - Listas Músicas
```
### Home

Neste é o link para a index do sistema

### Buscas

Neste, é onde há um submenu com 2 links, ambos levaram para uma página de upload de arquivos, 
e quando upar a música ele enviará para uma página onde listará as 10 ou a primeira (depende de qual dos links clicou) músicas achadas na API da Last.FM.
Nesta página haverá um (ou 10), formulários contendo os dados encontrados pela API 
(nome da música, artista, URL da last.fm, números de vezes que a música foi ouvida, e link da capa)
onde o usário pode escolher qual música melhor de adequa ou modificar os dados da musica que seram cadastrados.

<b>OBS:</b> caso o arquivo .mp3 já exista, ou não sejá encontrado nenhuma música com o nome enviado, mostrará um aviso e a pergunta se caso queira excluir aquele arquivo do sistema.

### Listas Música
Neste são lstadas todoas as músicas cadastradas no sistema, e mostrando todos os dados, incluindo o arquivo .mp3, e um botão para caso queira excluir algum dado do sistema.




## Observação para uso
Este aplicativo usa o arquivo .htaccess, verifique se seu servidor está liberado ou está permitindo usar o arquivo .htaccess, caso não esteja permitindo possivelmente este aplicativo não irá funcionar direito.
